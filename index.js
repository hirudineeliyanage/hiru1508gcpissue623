// This file is used to register all your cloud functions in GCP.
// DO NOT EDIT/DELETE THIS, UNLESS YOU KNOW WHAT YOU ARE DOING.

exports.hiru1508gcpissue623function = require("./hiru1508gcpissue623/function.js").handler;
exports.hiru1508gcpissue623hirutest = require("./hiru1508gcpissue623/hirutest.js").handler;